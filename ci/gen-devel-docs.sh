#!/bin/sh

set -eu

source /usr/local/python/bin/activate

mkdir -p public/devel-docs

sphinx-build docs public/devel-docs
echo "Copying in the spec"
cp docs/ipuz-spec.html public/devel-docs/
