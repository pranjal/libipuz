=============
Version 0.4.5
=============

Release date: December 27, 2023

This release adds IPuzCharset and IPuzPuzzleInfo. The former is useful
for indicating (and measuring) the characters in a puzzle. The latter
includes information and statistics about a puzzle.

This version has also had its license changed to LGPL-2.1/MIT.

## Changes
* Moved Charset from GNOME Crosswords to IPuzCharset
* Added IPuzPuzzleInfo

## Special thanks for this release

* Davide for docs fixes and Italian insight
* Federico for substantial CI fixes
* Tanmay for charset fixes and tests
* All prior contributors for permitting the license cleanup

=============
Version 0.4.4
=============

Release date: August 30, 2023

Paper bag release to match double-bumped version in crosswords.

=============
Version 0.4.3
=============

Release date: August 28, 2023

First release with an editable API. This lets us handle a wide-variety
of mutations to the puzzle. This version also adds acrostics, as well
as supports IPuzPuzzle::equal().

## Changes
* Add an editable API to IPuzCrossword:
    * ipuz_crossword_fix_numbering()
    * ipuz_crossword_fix_symmetry()
    * ipuz_crossword_fix_enumeration()
    * ipuz_crossword_fix_clues()
    * ipuz_crossword_fix_all()
* Add acrostic puzzle type
* Support clue_sets with the same direction but different labels
* New ipuz_puzzle_equal () function, and associated tests
* devel-docs available at libipuz.org. A copy of the ipuz spec is
  available as well.

## Special thanks for this release:

* Tanmay for acrostic support
* Pranjal for ipuz_puzzle_equal() support
* Federico, for massive advice on implementing edit support, and for
  help keeping our CI tests running.

=============
Version 0.4.2
=============

Release date: March 29, 2023

## Changes
* Add an Barred type crossword to make that style explicit
* build kind string into puzzles so that we can save different puzzle
  types.
* New function: ipuz_puzzle_kind_to/from_gtype()

=============
Version 0.4.1
=============

## Changes
* Update api_version to match release

=============
Version 0.4.0
=============

## Changes

* Big API refactor
    * Make Puzzle kinds their own subtype. Including Arrowword,
      Cryptic, and Filippine.
    * Move ClueId into libipuz from Crosswords as
      IPuzClueId. IPuzCrossword now has by_id variations for it's clue
      operations, where appropriate.
    * Add a concept of ClueSets to Crosswords, so that we no longer
      hardcode puzzles to Across/Down. We support puzzles with other
      column types (such as Clue/Hidden, for alphabetical crosswords
* Update copyright information
* Support Kind as per the spec. The old code wasn't sophisticated
  enough to handle what we would otherwise see.
* Initial devel website


=============
Version 0.3.0
=============

## Changes
* Make html handling spec-compliant, and work with the subset GMarkup
  can handle
* Change enumerations handling from a regex to a state machine to
  handle more edge cases. Add testing
* Make spec compliance more explicit
