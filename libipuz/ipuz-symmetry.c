/* ipuz-symmetry.c
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz.h"
#include <libipuz/ipuz-symmetry.h>

IPuzCellCoord
ipuz_symmetry_calculate (IPuzCellCoord      coord,
                         guint              puzzle_width,
                         guint              puzzle_height,
                         IPuzSymmetry       symmetry,
                         IPuzSymmetryOffset symmetry_offset)
{
  IPuzCellCoord mirror_coord = coord;

  /* We need to be within the puzzle bounds */
  g_return_val_if_fail (coord.row < puzzle_height && coord.column < puzzle_width, mirror_coord);
  /* Symmetry Quarter requires a square grid */
  if (puzzle_width != puzzle_height)
    g_return_val_if_fail (symmetry != IPUZ_SYMMETRY_ROTATIONAL_QUARTER, mirror_coord);

  switch (symmetry)
    {
    case IPUZ_SYMMETRY_NONE:
      break;
    case IPUZ_SYMMETRY_ROTATIONAL_HALF:
      mirror_coord.row = puzzle_height - coord.row - 1;
      mirror_coord.column = puzzle_width - coord.column - 1;
      break;
    case IPUZ_SYMMETRY_ROTATIONAL_QUARTER:
      /* Nested switch statements. Fun! */
      switch (symmetry_offset)
        {
        case IPUZ_SYMMETRY_OFFSET_OPPOSITE:
          mirror_coord.row = puzzle_height - coord.row - 1;
          mirror_coord.column = puzzle_width - coord.column - 1;
          break;
        case IPUZ_SYMMETRY_OFFSET_CW_ADJACENT:
          mirror_coord.row = coord.column;
          mirror_coord.column = puzzle_width - coord.row - 1;
          break;
        case IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT:
          mirror_coord.row = puzzle_height - coord.column - 1;
          mirror_coord.column = coord.row;
          break;
        default:
          g_assert_not_reached ();
        }
      break;
    case IPUZ_SYMMETRY_HORIZONTAL:
      mirror_coord.column = puzzle_width - coord.column - 1;
      break;
    case IPUZ_SYMMETRY_VERTICAL:
      mirror_coord.row = puzzle_height - coord.row - 1;
      break;
    case IPUZ_SYMMETRY_MIRRORED:
      switch (symmetry_offset)
        {
        case IPUZ_SYMMETRY_OFFSET_OPPOSITE:
          mirror_coord.row = puzzle_height - coord.row - 1;
          mirror_coord.column = puzzle_width - coord.column - 1;
          break;
        case IPUZ_SYMMETRY_OFFSET_CW_ADJACENT:
          mirror_coord.column = puzzle_width - coord.column - 1;
          break;
        case IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT:
          mirror_coord.row = puzzle_height - coord.row - 1;
          break;
        default:
          g_assert_not_reached ();
        }
      break;
    default:
      g_assert_not_reached ();
    }

  return mirror_coord;
}
