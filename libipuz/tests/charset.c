#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>

static void
ipuz_charset_handles_ascii (void)
{
  IPuzCharsetBuilder *builder = ipuz_charset_builder_new ();
  ipuz_charset_builder_add_text (builder, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

  g_autoptr (IPuzCharset) charset = ipuz_charset_builder_build (builder);

  g_assert_cmpint (ipuz_charset_get_n_chars (charset), ==, 26);
  g_assert_cmpint (ipuz_charset_get_char_index (charset, 'A'), ==, 0);
  g_assert_cmpint (ipuz_charset_get_char_index (charset, 'M'), ==, 12);
  g_assert_cmpint (ipuz_charset_get_char_index (charset, 'Z'), ==, 25);
  g_assert_cmpint (ipuz_charset_get_char_index (charset, '7'), ==, -1);

  g_autofree char *serialized = ipuz_charset_serialize (charset);
  g_assert_cmpstr (serialized, ==, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
}

static void
ipuz_charset_handles_non_ascii (void)
{
  IPuzCharsetBuilder *builder = ipuz_charset_builder_new ();

  ipuz_charset_builder_add_text (builder, "ÁRBOL");
  ipuz_charset_builder_add_text (builder, "BLÅHAJ");
  ipuz_charset_builder_add_text (builder, "BORLA");
  ipuz_charset_builder_add_text (builder, "TRALALA");

  g_autoptr (IPuzCharset) charset = ipuz_charset_builder_build (builder);

  g_assert_cmpint (ipuz_charset_get_n_chars (charset), ==, 10);
  g_autofree char *serialized = ipuz_charset_serialize (charset);

  /* Characters are sorted by Unicode code point, not logically.  I guess that doesn't matter? */
  g_assert_cmpstr (serialized, ==, "ABHJLORTÁÅ");

  g_assert_cmpint (ipuz_charset_get_char_index (charset, 'X'), ==, -1);
}

static void
ipuz_charset_serialization_roundtrip (void)
{
  IPuzCharsetBuilder *builder = ipuz_charset_builder_new ();

  ipuz_charset_builder_add_text (builder, "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ");

  g_autoptr (IPuzCharset) charset = ipuz_charset_builder_build (builder);

  g_autofree char *ser = ipuz_charset_serialize (charset);
  g_autoptr (IPuzCharset) deserialized = ipuz_charset_deserialize (ser);

  g_autofree char *roundtrip = ipuz_charset_serialize (deserialized);
  g_assert (strcmp (ser, roundtrip) == 0);
  g_assert (strcmp (roundtrip, "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ") == 0);
}

static void
histogram_support (void)
{
  IPuzCharsetBuilder *builder = ipuz_charset_builder_new ();

  ipuz_charset_builder_add_text (builder, "ABBCCCDDDDEEEEEFFFFFFGGGGGGG");

  g_autoptr (IPuzCharset) charset = ipuz_charset_builder_build (builder);

  g_assert (ipuz_charset_get_char_count (charset, g_utf8_get_char("E")) == 5);
}

static void
test_iters (void)
{
  IPuzCharsetBuilder *builder = ipuz_charset_builder_new ();
  IPuzCharsetIter *iter;
  guint n_chars = 0;

  ipuz_charset_builder_add_text (builder, "THETIMEHASCOMEFORALLGOODMENTOCOMETOTHEAIDOFTHEIRPARTY");

  g_autoptr (IPuzCharset) charset = ipuz_charset_builder_build (builder);

  for (iter = ipuz_charset_iter_first (charset);
       iter;
       iter= ipuz_charset_iter_next (iter))
    {
      gchar str[7];
      guint len;
      IPuzCharsetIterValue value;

      value = ipuz_charset_iter_get_value (iter);
      len = g_unichar_to_utf8 (value.c, str);
      str[len] = '\0';

      // g_print ("Char:%s:\tCount:%u:\n", str, value.count);

      if (!g_strcmp0 (str, "E"))
        g_assert (value.count == 7);
      n_chars ++;
    }

  /* Trust... */
  g_assert (n_chars == ipuz_charset_get_n_chars (charset));

  /* But verify. */
  g_assert (n_chars == 17);
}

static void
test_charset_remove_text (void)
{
  IPuzCharsetBuilder *builder = ipuz_charset_builder_new ();

  ipuz_charset_builder_add_text (builder, "AABBCCDDEEFFG");

  g_assert_false (ipuz_charset_builder_remove_text (builder , "TEXT"));

  g_assert_true (ipuz_charset_builder_remove_text (builder, "ABCDEFG"));

  g_autoptr (IPuzCharset) charset = ipuz_charset_builder_build (builder);

  g_assert_cmpint (ipuz_charset_get_size (charset), ==, 6);
}

static void
test_charset_equal_lang (void)
{
  g_autoptr (IPuzCharset) ipuz_charset_a = NULL;
  g_autoptr (IPuzCharset) ipuz_charset_b = NULL;

  ipuz_charset_a = ipuz_charset_builder_build (ipuz_charset_builder_new_for_language ("C"));
  ipuz_charset_b = ipuz_charset_builder_build (ipuz_charset_builder_new_for_language ("en"));

  g_assert (ipuz_charset_compare (ipuz_charset_a, ipuz_charset_b));

  ipuz_charset_unref (ipuz_charset_b);
  ipuz_charset_b = ipuz_charset_builder_build (ipuz_charset_builder_new_for_language ("es"));

  g_assert (! ipuz_charset_compare (ipuz_charset_a, ipuz_charset_b));
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/charset/handles_ascii", ipuz_charset_handles_ascii);
  g_test_add_func ("/charset/handles_non_ascii", ipuz_charset_handles_non_ascii);
  g_test_add_func ("/charset/serialization_roundtrip", ipuz_charset_serialization_roundtrip);
  g_test_add_func ("/charset/histogram", histogram_support);
  g_test_add_func ("/charset/iters", test_iters);

  g_test_add_func ("/charset/remove_text", test_charset_remove_text);

  g_test_add_func ("/charset/equal_lang", test_charset_equal_lang);

  return g_test_run ();
}
