#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>

static void
test_quarter_symmetry (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *path = NULL;
  g_autofree gchar *path_b = NULL;
  GArray *coords;
  IPuzCellCoord coord = {
    .row = 2,
    .column = 3,
  };
  IPuzCell *cell;

  setlocale(LC_ALL, "en_US.utf8");

  coords = g_array_new (FALSE, FALSE, sizeof (IPuzCellCoord));
  g_array_append_val (coords, coord);

  path = g_test_build_filename (G_TEST_DIST, "first-crossword.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  path_b = g_test_build_filename (G_TEST_DIST, "first-crossword-quarter.ipuz", NULL);
  puzzle_b = ipuz_puzzle_new_from_file (path_b, &error);
  g_assert (error == NULL);

  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle_a), coord);
  ipuz_cell_set_cell_type (cell, IPUZ_CELL_BLOCK);
  coord.row++;
  g_array_append_val (coords, coord);

  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle_a), coord);
  ipuz_cell_set_cell_type (cell, IPUZ_CELL_NULL);

  ipuz_crossword_fix_symmetry (IPUZ_CROSSWORD (puzzle_a), IPUZ_SYMMETRY_ROTATIONAL_QUARTER, coords);

  g_assert (ipuz_board_equal (ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_a)),
                              ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_b))));
  g_array_free (coords, TRUE);
}

static void
test_numbering (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *path = NULL;
  IPuzCellCoord coord = {
    .row = 2,
    .column = 3,
  };
  IPuzCell *cell;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "4962.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle_a), coord);
  ipuz_cell_set_cell_type (cell, IPUZ_CELL_BLOCK);

  coord.row = 5; coord.column = 3;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle_a), coord);
  ipuz_cell_set_cell_type (cell, IPUZ_CELL_NORMAL);

  ipuz_crossword_fix_numbering (IPUZ_CROSSWORD (puzzle_a));
  coord.row = 14; coord.column = 5;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle_a), coord);
  g_assert (ipuz_cell_get_number (cell) == 30);
}

static void
test_clues (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *path = NULL;
  IPuzCellCoord coord = {
    .row = 2,
    .column = 3,
  };
  IPuzCell *cell;
  IPuzClue *clue;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "4962.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle_a), coord);
  ipuz_cell_set_cell_type (cell, IPUZ_CELL_BLOCK);

  coord.row = 5; coord.column = 3;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle_a), coord);
  ipuz_cell_set_cell_type (cell, IPUZ_CELL_NORMAL);

  ipuz_crossword_fix_numbering (IPUZ_CROSSWORD (puzzle_a));
  ipuz_crossword_fix_clues (IPUZ_CROSSWORD (puzzle_a));
  coord.row = 14; coord.column = 5;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle_a), coord);
  g_assert (ipuz_cell_get_number (cell) == 30);
  clue = ipuz_crossword_find_clue_by_number (IPUZ_CROSSWORD (puzzle_a),
                                             IPUZ_CLUE_DIRECTION_ACROSS,
                                             9);
  g_assert (ipuz_clue_get_clue_text (clue) == NULL);
}

static void
test_all (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *path = NULL;
  GArray *coords;
  IPuzCellCoord coord = {
    .row = 2,
    .column = 3,
  };
  IPuzCell *cell;
  IPuzClue *clue;

  setlocale(LC_ALL, "en_US.utf8");
  coords = g_array_new (FALSE, FALSE, sizeof (IPuzCellCoord));
  g_array_append_val (coords, coord);

  path = g_test_build_filename (G_TEST_DIST, "4962.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle_a), coord);
  ipuz_cell_set_cell_type (cell, IPUZ_CELL_BLOCK);

  coord.row = 5; coord.column = 3;
  g_array_append_val (coords, coord);
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle_a), coord);
  ipuz_cell_set_cell_type (cell, IPUZ_CELL_NORMAL);

  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle_a),
                          "symmetry", IPUZ_SYMMETRY_ROTATIONAL_HALF,
                          "symmetry-coords", coords,
                          NULL);

  ipuz_crossword_print (IPUZ_CROSSWORD (puzzle_a));

  coord.row = 14; coord.column = 5;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle_a), coord);
  g_assert (ipuz_cell_get_number (cell) == 32);
  clue = ipuz_crossword_find_clue_by_number (IPUZ_CROSSWORD (puzzle_a),
                                             IPUZ_CLUE_DIRECTION_ACROSS,
                                             9);
  g_assert (ipuz_clue_get_clue_text (clue) == NULL);

  g_array_free (coords, TRUE);
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/crossword/quarter_symmetry", test_quarter_symmetry);
  g_test_add_func ("/crossword/half_symmetry", test_numbering);
  g_test_add_func ("/crossword/clues", test_clues);
  g_test_add_func ("/crossword/all", test_all);

  return g_test_run ();
}

