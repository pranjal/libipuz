/* ipuz-symmetry.h
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-cell.h>

G_BEGIN_DECLS


typedef enum
{
  IPUZ_SYMMETRY_NONE,               /* @ Freeform grid @ */
  IPUZ_SYMMETRY_ROTATIONAL_HALF,    /* @ 180° Rotational Symmetry around the center @ */
  IPUZ_SYMMETRY_ROTATIONAL_QUARTER, /* @ 90° Rotational Symmetry around the center. Only works on square boards. @ */
  IPUZ_SYMMETRY_HORIZONTAL,         /* @ Symmetry across the horizontal center line @ */
  IPUZ_SYMMETRY_VERTICAL,           /* @ Symmetry across the vertical center line @ */
  IPUZ_SYMMETRY_MIRRORED,           /* @ Symmetry across the vertical and vertical center lines @ */
} IPuzSymmetry;

/* Some symmetries have four points of symmetry, others two. When
 * trying to calculate the symmetry, this detail can be used to
 * indicate which one is being requested.
 *
 * The following types can have four points of symmetry:
 * IPUZ_SYMMETRY_ROTATIONAL_QUARTER
 * IPUZ_SYMMETRY_MIRRORED
 */
typedef enum
{
  IPUZ_SYMMETRY_OFFSET_OPPOSITE,     /* @ Opposite cell across the mode of symmetry @ */
  IPUZ_SYMMETRY_OFFSET_CW_ADJACENT,  /* @ Clockwise adjacent point @ */
  IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT, /* @ Counter-clockwise adjacent point @ */
} IPuzSymmetryOffset;


IPuzCellCoord  ipuz_symmetry_calculate (IPuzCellCoord      coord,
                                        guint              puzzle_width,
                                        guint              puzzle_height,
                                        IPuzSymmetry       symmetry,
                                        IPuzSymmetryOffset symmetry_offset);


G_END_DECLS
