/* ipuz-board.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <libipuz/ipuz-clue.h>
#include <libipuz/ipuz-cell.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_BOARD (ipuz_board_get_type ())
G_DECLARE_FINAL_TYPE (IPuzBoard, ipuz_board, IPUZ, BOARD, GObject)

IPuzBoard *ipuz_board_new              (void);
void       ipuz_board_build_puzzle     (IPuzBoard     *board,
                                        JsonBuilder   *builder,
                                        const gchar   *block,
                                        const gchar   *empty);
void       ipuz_board_build_solution   (IPuzBoard     *board,
                                        JsonBuilder   *builder,
                                        const gchar   *block);
gboolean   ipuz_board_equal            (IPuzBoard     *a,
                                        IPuzBoard     *b);
void       ipuz_board_resize           (IPuzBoard     *board,
                                        guint          new_width,
                                        guint          new_height);
void       ipuz_board_parse_puzzle     (IPuzBoard     *board,
                                        JsonNode      *node,
                                        const gchar   *block,
                                        const gchar   *empty);
void       ipuz_board_parse_solution   (IPuzBoard     *board,
                                        JsonNode      *node,
                                        const gchar   *block,
                                        const gchar   *charset);

guint      ipuz_board_get_width        (IPuzBoard     *board);
guint      ipuz_board_get_height       (IPuzBoard     *board);
IPuzCell  *ipuz_board_get_cell         (IPuzBoard     *board,
                                        IPuzCellCoord  coord);
IPuzCell  *ipuz_board_get_cell_by_clue (IPuzBoard     *board,
                                        IPuzClue      *clue,
                                        IPuzCellCoord *coord);


G_END_DECLS
