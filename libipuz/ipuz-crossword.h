/* ipuz-crossword.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-puzzle.h>
#include <libipuz/ipuz-board.h>
#include <libipuz/ipuz-charset.h>
#include <libipuz/ipuz-clue.h>
#include <libipuz/ipuz-guesses.h>
#include <libipuz/ipuz-symmetry.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_CROSSWORD (ipuz_crossword_get_type ())
G_DECLARE_DERIVABLE_TYPE    (IPuzCrossword, ipuz_crossword, IPUZ, CROSSWORD, IPuzPuzzle);


typedef void (*IPuzCrosswordForeachCellFunc) (IPuzCrossword *crossword,
                                              IPuzCell      *cell,
                                              IPuzCellCoord  coord,
                                              gpointer       user_data);


/* Indication of where to physically place the clues on a playing
 * board compared to the grid */
typedef enum
{
  IPUZ_CLUE_PLACEMENT_NULL,   /* @ Auto place clues @ */
  IPUZ_CLUE_PLACEMENT_BEFORE, /* @ Put clues before the puzzle @ */
  IPUZ_CLUE_PLACEMENT_AFTER,  /* @ Put clues after the puzzle @ */
  IPUZ_CLUE_PLACEMENT_BLOCKS, /* @ Put clues in blocks adjacent to entry @ */
} IPuzCluePlacement;


typedef void (*IPuzClueForeachFunc) (IPuzClueDirection  direction,
                                     IPuzClue          *clue,
                                     IPuzClueId         clue_id,
                                     gpointer           user_data);

typedef struct _IPuzCrosswordClass IPuzCrosswordClass;
struct _IPuzCrosswordClass
{
  IPuzPuzzleClass parent_class;

  void     (*fix_symmetry)         (IPuzCrossword      *self,
                                    IPuzSymmetry        symmetry,
                                    GArray             *symmetry_coords);
  void     (*fix_numbering)        (IPuzCrossword      *self);
  void     (*fix_clues)            (IPuzCrossword      *self);
  void     (*fix_enumerations)     (IPuzCrossword      *self);
  void     (*fix_styles)           (IPuzCrossword      *self);
  void     (*fix_all)              (IPuzCrossword      *self,
                                    const gchar        *first_attribute_name,
                                    va_list             var_args);
  gboolean (*clue_continues_up)    (IPuzCrossword      *self,
                                    IPuzCellCoord       coord);
  gboolean (*clue_continues_down)  (IPuzCrossword      *self,
                                    IPuzCellCoord       coord);
  gboolean (*clue_continues_left)  (IPuzCrossword      *self,
                                    IPuzCellCoord       coord);
  gboolean (*clue_continues_right) (IPuzCrossword      *self,
                                    IPuzCellCoord       coord);
  void     (*mirror_cell)          (IPuzCrossword      *self,
                                    IPuzCellCoord       src_coord,
                                    IPuzCellCoord       dest_coord,
                                    IPuzSymmetry        symmetry,
                                    IPuzSymmetryOffset  symmetry_offset);
  gboolean (*check_mirror)         (IPuzCrossword      *self,
                                    IPuzCellCoord       src_coord,
                                    IPuzCellCoord       target_coord,
                                    IPuzSymmetry        symmetry,
                                    IPuzSymmetryOffset  symmetry_offset);
};


IPuzCrossword     *ipuz_crossword_new                    (void);
guint              ipuz_crossword_get_width              (IPuzCrossword                *self);
guint              ipuz_crossword_get_height             (IPuzCrossword                *self);
void               ipuz_crossword_set_size               (IPuzCrossword                *self,
                                                          gint                          width,
                                                          gint                          height);
IPuzBoard         *ipuz_crossword_get_board              (IPuzCrossword                *self);
gboolean           ipuz_crossword_set_guesses            (IPuzCrossword                *self,
                                                          IPuzGuesses                  *guesses);
IPuzGuesses       *ipuz_crossword_get_guesses            (IPuzCrossword                *self);
IPuzCell          *ipuz_crossword_get_cell               (IPuzCrossword                *self,
                                                          IPuzCellCoord                 coord);
void               ipuz_crossword_foreach_cell           (IPuzCrossword                *self,
                                                          IPuzCrosswordForeachCellFunc  func,
                                                          gpointer                      user_data);
IPuzCharset       *ipuz_crossword_get_solution_chars     (IPuzCrossword                *self);
IPuzSymmetry       ipuz_crossword_get_symmetry           (IPuzCrossword                *self);
IPuzCellCoord      ipuz_crossword_get_offset_coord       (IPuzCrossword                *self,
                                                          IPuzCellCoord                 coord,
                                                          IPuzSymmetry                  symmetry,
                                                          IPuzSymmetryOffset            offset_type);

/* Clue sets */
guint              ipuz_crossword_get_n_clue_sets        (IPuzCrossword                *self);
IPuzClueDirection  ipuz_crossword_clue_set_get_dir       (IPuzCrossword                *self,
                                                          guint                         index);
const gchar       *ipuz_crossword_clue_set_get_label     (IPuzCrossword                *self,
                                                          IPuzClueDirection             direction);

/* Clues */
GArray            *ipuz_crossword_get_clues              (IPuzCrossword                *self,
                                                          IPuzClueDirection             direction);
void               ipuz_crossword_foreach_clue           (IPuzCrossword                *self,
                                                          IPuzClueForeachFunc           func,
                                                          gpointer                      data);
guint              ipuz_crossword_get_n_clues            (IPuzCrossword                *self,
                                                          IPuzClueDirection             direction);
IPuzClue          *ipuz_crossword_get_clue_by_id         (IPuzCrossword                *self,
                                                          IPuzClueId                    clue_id);
void               ipuz_crossword_unlink_clue            (IPuzCrossword                *self,
                                                          IPuzClue                     *clue);
IPuzClueId         ipuz_crossword_get_clue_id            (IPuzCrossword                *self,
                                                          const IPuzClue               *clue);
gchar             *ipuz_crossword_get_clue_string_by_id  (IPuzCrossword                *self,
                                                          IPuzClueId                    clue_id);
gchar             *ipuz_crossword_get_guess_string_by_id (IPuzCrossword                *self,
                                                          IPuzClueId                    clue_id);
gboolean           ipuz_crossword_clue_guessed           (IPuzCrossword                *self,
                                                          IPuzClue                     *clue,
                                                          gboolean                     *correct);
guint              ipuz_crossword_find_clue              (IPuzCrossword                *self,
                                                          IPuzClue                     *clue);
IPuzClue          *ipuz_crossword_find_clue_by_number    (IPuzCrossword                *self,
                                                          IPuzClueDirection             direction,
                                                          gint                          number);
IPuzClue          *ipuz_crossword_find_clue_by_label     (IPuzCrossword                *self,
                                                          IPuzClueDirection             direction,
                                                          const char                   *label);
IPuzClue          *ipuz_crossword_find_clue_by_coord     (IPuzCrossword                *self,
                                                          IPuzClueDirection             direction,
                                                          IPuzCellCoord                 coord);

/* Game state */
gboolean           ipuz_crossword_game_won               (IPuzCrossword                *self);

/* Correction API */
void               ipuz_crossword_fix_symmetry           (IPuzCrossword                *self,
                                                          IPuzSymmetry                  symmetry,
                                                          GArray                       *symmetry_coords);
void               ipuz_crossword_fix_numbering          (IPuzCrossword                *self);
void               ipuz_crossword_fix_clues              (IPuzCrossword                *self);
void               ipuz_crossword_fix_enumerations       (IPuzCrossword                *self);
void               ipuz_crossword_fix_styles             (IPuzCrossword                *self);
void               ipuz_crossword_fix_all                (IPuzCrossword                *self,
                                                          const char                   *first_attribute_name,
                                                          ...);
gboolean           ipuz_crossword_clue_continues_up      (IPuzCrossword                *self,
                                                          IPuzCellCoord                 coord);
gboolean           ipuz_crossword_clue_continues_down    (IPuzCrossword                *self,
                                                          IPuzCellCoord                 coord);
gboolean           ipuz_crossword_clue_continues_left    (IPuzCrossword                *self,
                                                          IPuzCellCoord                 coord);
gboolean           ipuz_crossword_clue_continues_right   (IPuzCrossword                *self,
                                                          IPuzCellCoord                 coord);
void               ipuz_crossword_mirror_cell            (IPuzCrossword                *self,
                                                          IPuzCellCoord                 src_coord,
                                                          IPuzCellCoord                 dest_coord,
                                                          IPuzSymmetry                  symmetry,
                                                          IPuzSymmetryOffset            symmetry_offset);
gboolean           ipuz_crossword_check_mirror           (IPuzCrossword                *self,
                                                          IPuzCellCoord                 src_coord,
                                                          IPuzCellCoord                 target_coord,
                                                          IPuzSymmetry                  symmetry,
                                                          IPuzSymmetryOffset            symmetry_offset);

/* Public functions */
void               ipuz_crossword_print                  (IPuzCrossword                *self);



G_END_DECLS
