/* ipuz-enumeration.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#include "libipuz-config.h"
#include "ipuz-enumeration.h"
#include <glib/gi18n-lib.h>


#define CHAR_IS_MODIFIER(c)  (c == '*' || c == '^' || c == '+')
#define CHAR_IS_SEPARATOR(c) (c == '.' || c == '-' || c == ' ' || c == ',' || c == '\'')

struct _IPuzEnumeration
{
  grefcount ref_count;
  IPuzVerbosity verbosity;
  gchar *src;
  gchar *display;
  GList *delims;
};

typedef struct _DelimSegment
{
  guint grid_offset;
  IPuzDeliminator delim;
} DelimSegment;


G_DEFINE_BOXED_TYPE (IPuzEnumeration, ipuz_enumeration, ipuz_enumeration_ref, ipuz_enumeration_unref);


static GList *
append_segment (GList           *list,
                guint            grid_offset,
                IPuzDeliminator  delim)
{
  DelimSegment *segment = g_new0 (DelimSegment, 1);

  segment->grid_offset = grid_offset;
  segment->delim = delim;
  //  g_print ("\tAppending %u %d\n", grid_offset, (gint) delim);
  return g_list_append (list, segment);
}

#define MAX_ENUMERATION_SIZE 999

typedef enum
{
  LOOK_FOR_WORD,
  LOOK_FOR_NUMBER,
  LOOK_FOR_SEPARATOR,
} DelimState;

/* This parses the src string.
 *
 * We go through cycles of looking for (optionally) a modifier, then a
 * word, then a separator
 */


static void
create_delim_list (IPuzEnumeration *enumeration)
{
  gchar *ptr = enumeration->src;
  guint grid_offset = 0;
  DelimState state;

  /* Handle the empty string */
  if (! *ptr)
    goto error;

  /* peek at the first character to make sure we're looking at t */
  if (CHAR_IS_MODIFIER (*ptr) || g_ascii_isdigit (*ptr))
    {
      grid_offset++;
      state = LOOK_FOR_WORD;
    }
  else if (CHAR_IS_SEPARATOR (*ptr))
    state = LOOK_FOR_SEPARATOR;
  else
    goto error;

  while (*ptr)
    {
      /* Check for modifiers */
      if (state == LOOK_FOR_WORD &&
          CHAR_IS_MODIFIER (*ptr))
        {
          IPuzDeliminator delim;

          if (*ptr == '*') delim = IPUZ_DELIMINATOR_ALLCAPS;
          else if (*ptr == '^') delim = IPUZ_DELIMINATOR_CAPITALIZED;
          else if (*ptr == '+') delim = IPUZ_DELIMINATOR_FOREIGN;
          else g_assert_not_reached ();

          enumeration->delims =
            append_segment (enumeration->delims, grid_offset, delim);
          state = LOOK_FOR_NUMBER;
          ptr = g_utf8_next_char (ptr);
          continue;
        }

      if ((state == LOOK_FOR_WORD || state == LOOK_FOR_NUMBER) &&
          (g_ascii_isdigit (*ptr)))
        {
          guint64 num;
          gchar *endptr = NULL;
          num = g_ascii_strtoll (ptr,
                                 &endptr,
                                 10);
          /* We limit ourselves to two digits per word */
          /* We don't handle enumerations of 0, right now. */
          if (num > MAX_ENUMERATION_SIZE || num == 0)
            goto error;
          grid_offset += num * 2 - 1;
          ptr = endptr;
          state = LOOK_FOR_SEPARATOR;
          continue;
        }

      if (state == LOOK_FOR_SEPARATOR &&
          CHAR_IS_SEPARATOR (*ptr))
        {
          IPuzDeliminator delim;

          if (*ptr == ' ') delim = IPUZ_DELIMINATOR_WORD_BREAK;
          else if (*ptr == ',') delim = IPUZ_DELIMINATOR_WORD_BREAK;
          else if (*ptr == '-') delim = IPUZ_DELIMINATOR_DASH;
          else if (*ptr == '.') delim = IPUZ_DELIMINATOR_PERIOD;
          else if (*ptr == '\'') delim = IPUZ_DELIMINATOR_APOSTROPHE;
          else g_assert_not_reached ();

          enumeration->delims =
            append_segment (enumeration->delims, grid_offset, delim);
          state = LOOK_FOR_WORD;
          grid_offset++;
          ptr = g_utf8_next_char (ptr);
          continue;
        }
      /* If we don't fit this pattern, we error out and just use the src */
      goto error;
    }

  /* We are okay ending with a separator or a number. We are not okay
     ending with a modifier. So '5-' ("ACETO-") or '-5' are fine, but
     '5+' is not.
   */
  if (state != LOOK_FOR_NUMBER)
    {
      /* If we end with a separator we need to wind back the
         grid_offset, as we advanced beyond the end */
      if (state == LOOK_FOR_WORD)
        grid_offset--;
      enumeration->delims = append_segment (enumeration->delims, grid_offset,
                                            IPUZ_DELIMINATOR_WORD_BREAK);
      return;
    }

 error:
  /* If we hit an error mid-parse it means we don't fit our
   * pattern. We bail out and just go with the src string for the
   * display string */
  enumeration->display = g_strdup (enumeration->src);
  g_list_free_full (enumeration->delims, g_free);
  enumeration->delims = NULL;
}

static void
create_display (IPuzEnumeration *enumeration)
{
  GList *list;
  guint grid_offset = 0;
  GString *display;
  gboolean word_modifier = FALSE;
  IPuzDeliminator word_modifier_type = IPUZ_DELIMINATOR_WORD_BREAK; /* dummy value; will get initialized later */

  g_assert (enumeration);
  g_clear_pointer (&enumeration->display, g_free);

  /* Special case when we couldn't parse the enumeration. Just repeat it */
  if (enumeration->delims == NULL)
    {
      enumeration->display = g_strdup (enumeration->src);
      return;
    }

  display = g_string_new (NULL);

  for (list = enumeration->delims; list; list = list->next)
    {
      DelimSegment *segment = (DelimSegment *) list->data;

      if (DELIM_IS_MODIFIER (segment->delim))
        {
          word_modifier = TRUE;
          word_modifier_type = segment->delim;
          continue;
        }
      if (segment->grid_offset - grid_offset > 0)
        g_string_append_printf (display, "%d", (segment->grid_offset - grid_offset)/2);
      if (word_modifier)
        {
          word_modifier = FALSE;
          switch (word_modifier_type)
            {
            case IPUZ_DELIMINATOR_ALLCAPS:
              g_string_append (display, _(" (allcaps)"));
              break;
            case IPUZ_DELIMINATOR_CAPITALIZED:
              g_string_append (display, _(" (capitalized)"));
              break;
            case IPUZ_DELIMINATOR_FOREIGN:
              g_string_append (display, _(" (foreign)"));
              break;
            default:
              g_assert_not_reached ();
            }
        }
      grid_offset = segment->grid_offset;
      if (list->next != NULL)
        {
          switch (segment->delim)
            {
            case IPUZ_DELIMINATOR_WORD_BREAK:
              g_string_append (display, ",");
              break;
            case IPUZ_DELIMINATOR_PERIOD:
              g_string_append (display, ".");
              break;
            case IPUZ_DELIMINATOR_DASH:
              g_string_append (display, "-");
              break;
            case IPUZ_DELIMINATOR_APOSTROPHE:
              g_string_append (display, "\'");
              break;
            default:
              break;
            }
        }
    }
  enumeration->display = g_string_free (display, FALSE);
}

IPuzEnumeration *
ipuz_enumeration_new (const gchar   *src,
                      IPuzVerbosity  verbosity)
{
  IPuzEnumeration *enumeration;

  g_return_val_if_fail (src != NULL, NULL);

  enumeration = g_new0 (IPuzEnumeration, 1);
  g_ref_count_init (&enumeration->ref_count);
  enumeration->src = g_strdup (src);
  enumeration->verbosity = verbosity;
  create_delim_list (enumeration);
  create_display (enumeration);

  return enumeration;
}

IPuzEnumeration *
ipuz_enumeration_dup (const IPuzEnumeration *enumeration)
{
  if (enumeration == NULL)
    return NULL;

  return ipuz_enumeration_new (enumeration->src, enumeration->verbosity);
}

IPuzEnumeration *
ipuz_enumeration_ref (IPuzEnumeration *enumeration)
{
  g_return_val_if_fail (enumeration != NULL, NULL);

  g_ref_count_inc (&enumeration->ref_count);

  return enumeration;
}

void
ipuz_enumeration_unref (IPuzEnumeration *enumeration)
{
  if (enumeration == NULL)
    return;

  if (!g_ref_count_dec (&enumeration->ref_count))
    return;

  g_free (enumeration->src);
  g_free (enumeration->display);
  g_list_free_full (enumeration->delims, g_free);
  g_free (enumeration);
}

gboolean
ipuz_enumeration_equal (const IPuzEnumeration *enumeration1,
                        const IPuzEnumeration *enumeration2)
{
  if (enumeration1 == NULL && enumeration2 == NULL)
    return TRUE;

  if (enumeration1 == NULL || enumeration2 == NULL)
    return FALSE;

  return ! g_strcmp0 (enumeration1->src, enumeration2->src);
}

const gchar *
ipuz_enumeration_get_src (const IPuzEnumeration *enumeration)
{
  g_return_val_if_fail (enumeration != NULL, NULL);

  return enumeration->src;
}

const gchar *
ipuz_enumeration_get_display (const IPuzEnumeration *enumeration)
{
  g_return_val_if_fail (enumeration != NULL, NULL);

  return enumeration->display;
}

/**
 * ipuz_enumeration_get_has_delim:
 * @enumeration: an @IPuzEnumeration
 *
 * Returns whether an @enumeration has valid delims within it. As an
 * example, an enumeration of "4-4,3" would return %TRUE, while an
 * enumeration of "two words" would return %FALSE.
 *
 * Returns: whether @enumeration has valid delims
 **/
gboolean
ipuz_enumeration_get_has_delim (const IPuzEnumeration *enumeration)
{
  g_return_val_if_fail (enumeration != NULL, FALSE);

  return (enumeration->delims != NULL);
}

/**
 * ipuz_enumeration_delim_length:
 * @enumeration: an @IPuzEnumeration
 *
 * Returns the length of @enumeration in cells. As an example, an
 * enumeration of "4-4,3" would return a length of 11. A length of 0
 * indicates an inditerminate length. A length of -1 indicates that
 * the source string for the enumeration wasn't parseable.
 *
 * Returns: the length of @enumeration
 **/
gint
ipuz_enumeration_delim_length (const IPuzEnumeration *enumeration)
{
  GList *list;

  g_return_val_if_fail (enumeration != NULL, -1);

  list = g_list_last (enumeration->delims);
  if (list)
    {
      DelimSegment *segment = list->data;

      return segment->grid_offset / 2;
    }

  return -1;
}

void
ipuz_enumeration_delim_foreach (const IPuzEnumeration *enumeration,
                                IPuzDelimFunc          func,
                                gpointer               user_data)
{
  GList *list;

  g_return_if_fail (enumeration != NULL);

  for (list = enumeration->delims; list; list = list->next)
    {
      DelimSegment *segment = list->data;

      (*func) (segment->delim,
               segment->grid_offset,
               (list->next == NULL),
               user_data);
    }
}

gboolean
ipuz_enumeration_valid_char (gchar c)
{
  return CHAR_IS_SEPARATOR (c) || CHAR_IS_MODIFIER (c) || g_ascii_isdigit (c);
}
