# Full Puzzle Kind
Status: **Draft**

A proposed inheritance tree for libipuz. Some open questions:

* Q. Should **ACROSTIC** and **FILL_IN** inherit from crossword? They
  separate in the spec and are generally quite different puzzle
  types. However, the boards they have are basically identical, so as
  a practical matter making them crosswords could be useful.

  An argument for making them separate is the invariants enforced by
  the `fix_()` style functions are totally different. None of the ones
  in `IPuzCrossword` will apply to `IPuzCryptic`, while eg. the ones
  in `IPuzArrowword` and `IPuzBarred` apply in spirit (if not code).

* Q. Which of these can share the same `IPuzCell` type? Should that
  class grow to be the union of all the values, or do we want to
  investigate different types? As an example, do we store numbers for
  sudokus, or just use a string?


| Class                        | Puzzle Kind             | Kind URI                                   | Implemented | Notes                        |
|------------------------------|-------------------------|--------------------------------------------|-------------|------------------------------|
| `IPuzPuzzle`                 | *None*                  | *None*                                     | [X]         | Base class for all puzzles   |
| ┣ `IPuzAnswer`               | **ANSWER**              | http://ipuz.org/answer                     |             |                              |
| ┃  ┣ `IPuzRiddle`            | **RIDDLE**              | http://ipuz.org/answer/riddle              |             |                              |
| ┃  ┣ `IPuzChoice`            | **CHOICE**              | http://ipuz.org/answer/choice              |             |                              |
| ┃  ┗ `IPuzFlat`              | **FLAT**                | http://ipuz.org/answer/flat                |             |                              |
| ┣ `IPuzBlock`                | **BLOCK**               | http://ipuz.org/block                      |             |                              |
| ┃  ┣ `IPuzSlidingBlock`      | **SLIDING_BLOCK**       | http://ipuz.org/block/slidingblock         |             |                              |
| ┃  ┗ `IPuzMaze`              | **MAZE**                | http://ipuz.org/block/maze                 |             |                              |
| ┣ `IPuzCrossword`            | **CROSSWORD**           | http://ipuz.org/crossword                  | [X]         |                              |
| ┃  ┣ `IPuzArrowword`         | **ARROWWORD**           | http://ipuz.org/crossword/arrowword        | [X]         |                              |
| ┃  ┣ `IPuzAcrostic`          | **ACROSTIC**            | http://ipuz.org/acrostic                   | [X]         | Currently subtypes Crossword |
| ┃  ┣ `IPuzBarred`            | **BARRED**              | https://libipuz.org/barred                 | [X]         | libipuz extension            |
| ┃  ┣ `IPuzCryptic`           | **CRYPTIC**             | http://ipuz.org/crossword/crypticcrossword | [X]         |                              |
| ┃  ┣ `IPuzDiagramless`       | **DIAGRAMLESS**         | http://ipuz.org/crossword/diagramless      |             |                              |
| ┃  ┣ `IPuzFilippine`         | **FILIPPINE**           | https://libipuz.org/filippine              | [X]         | libipuz extension            |
| ┃  ┗ `IPuzFillIn`            | **FILL_IN**             | http://ipuz.org/fill                       |             |                              |
| ┃  ┗ `IPuzFreeForm`          | **FREE_FORM**           | https://libipuz.org/freeform               |             | libipuz extension            |
| ┣ `IPuzSudoku`               | **SUDOKU**              | http://ipuz.org/sudoku                     |             |                              |
| ┃  ┣ `IPuzCalcudoku`         | **CALCUDOKU**           | http://ipuz.org/sudoku/calcudoku           |             |                              |
| ┃  ┣ `IPuzDiagonalSudoku`    | **DIAGONAL_SUDOKU**     | http://ipuz.org/sudoku/diagonalsudoku      |             |                              |
| ┃  ┣ `IPuzHyperSudoku`       | **HYPER_SUDOKU**        | http://ipuz.org/sudoku/hypersudoku         |             |                              |
| ┃  ┣ `IPuzJigsawSudoku`      | **JIGSAW_SUDOKU**       | http://ipuz.org/sudoku/jigsawsudoku        |             |                              |
| ┃  ┣ `IPuzGreaterThanSudoku` | **GREATER_THAN_SUDOKU** | http://ipuz.org/greaterthansudoku          |             | URI is missing sudoku/       |
| ┃  ┣ `IPuzKillerSudoku`      | **KILLER_SUDOKU**       | http://ipuz.org/sudoku/killersudoku        |             |                              |
| ┃  ┣ `IPuzLatinSquare`       | **LATIN_SQUARE**        | http://ipuz.org/sudoku/latinsquare         |             |                              |
| ┃  ┗ `IPuzWordoku`           | **WORDOKU**             | http://ipuz.org/sudoku/wordoku             |             |                              |
| ┗ `IPuzWordSearch`           | **WORD_SEARCH**         | http://ipuz.org/wordsearch                 |             |                              |
| ┣ `IPuzAnagrid`              | **ANAGRID**             | http://ipuz.org/wordsearch/anagrid         |             |                              |
| ┗ `IPuzTraceOut`             | **TRACE_OUT**           | http://ipuz.org/wordsearch/traceout        |             |                              |

