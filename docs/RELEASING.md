# Release process checklist for libipuz

## Before beginning
- [ ] Refresh your memory with
      https://wiki.gnome.org/MaintainersCorner/Releasing
- [ ] Update the overview files and any other devel docs that need
      updating.
- [ ] Update README.md with any changes
- [ ] Go through outstanding issues! Make sure we're ready to go and
not forgetting something.
- [ ] Make sure we pass all tests! `ninja test -C _build`
- [ ] Update `NEWS.md`, see below for the preferred format and hints.
- [ ] Update `index.md` to indicate latest release version

## Release the source
- [ ] Commit the changes above.
- [ ] Build it one more time
- [ ] Create a release with `meson dist` in the _build directory. This
won't work if there are uncommitted changes.
- [ ] Create a signed tag: `git tag -s x.y.z` with the version number.
- [ ] `git push origin master`
- [ ] `git push origin x.y.z` the signed tag
- [ ] Create a [release in Gitlab](#gitlab-release).

## Gitlab release

- [ ] Go to https://gitlab.gnome.org/jrb/libipuz/-/releases and click
the **New release** button.
- [ ] Select the tag `x.y.z` you created as part of the release steps.
- [ ] If there is an associated milestone, select it too.
- [ ] Fill in the release title - `x.y.z - stable` or `x.y.z -
development`.
- [ ] Copy the release notes from NEWS.md.

## Post release

- [ ] Bump the version immediately post-release, and push it to
      master. Any API additions will require a new version of this
      library.


## Format for release notes in NEWS

The `NEWS` file contains the release notes.  Please use something
close to this format; it is not mandatory, but makes the formatting
consistent, and is what tooling expects elsewhere - also by writing
Markdown, you can just cut&paste it into a Gitlab release.  You can
skim bits of the NEWS file for examples on style and content.

New entries go at the **top** of the file.

An easy way to double check you got all the news is to run `git log
x.y.z..@ --oneline`, where `x.y.z` is the last release tag.

Example News

```markdown
=============
Version x.y.z
=============

Release date: Month xx, 20xx

Optional Summary

## Changes: libipuz x.y.z
* Update 1
* Update 2
* Update 3

## Special thanks for this release:

- Any people that you want to highlight.
```
