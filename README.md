Library for parsing .ipuz puzzle files.

See http://ipuz.org/ ([local
mirror](https://libipuz.org/ipuz-spec.html)) for more information
about the file format.

*ipuz is a trademark of Puzzazz, Inc., used with permission*

## Development

Please see the [Development
guide](https://jrb.pages.gitlab.gnome.org/libipuz/devel-docs/index.html)
for information on building and running the code.


## License

This work is dual-licensed under the LGPL 2.1 (or any later version)
and the MIT License.  You can choose between one of them if you use
this work.

`SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)`
